#include "TestSuite1.h"
#include "Assert.h"

TestSuite1::TestSuite1()
	:TestSuite(std::string("Test Suite 1"))
{
	add_test(Test(std::string("Empty test"), std::bind(&TestSuite1::empty_test, this)));
	add_test(Test(std::string("Equal integers test"), std::bind(&TestSuite1::equal_integers_test, this)));
}

void TestSuite1::empty_test()
{

}

void TestSuite1::equal_integers_test()
{
	Assert::are_equal<int>(1, 1);
}
