#include <iostream>

#include "TestApplicationTests.h"
#include "ConsoleTestPresenter.h"

int main()
{
	auto presenter = ConsoleTestPresenter();
	auto app_tests = TestApplicationTests(presenter);
	app_tests.run();
}
