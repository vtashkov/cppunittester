#pragma once
#include <exception>

class Assert
{
public:
	template<class T> 
	static void are_equal(const T & expected, const T & actual)
	{
		if (expected != actual)
			throw std::exception("Fail!");
	}

	template<class T> 
	static void are_not_equal(const T & expected, const T & actual)
	{
		if (expected == actual)
			throw std::exception("Fail!");
	}

	static void is_true(const bool& expected);
	static void is_false(const bool& expected);
	static void is_null(const void* expected);
	static void is_not_null(const void* expected);
};

