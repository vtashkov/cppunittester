#include "TestSuite.h"

TestSuite::TestSuite(const std::string &name) : name(name)
{
}

std::string TestSuite::get_name() const
{
	return name;
}

int TestSuite::get_tests_count() const
{
	return tests.size();
}

std::vector<Test> TestSuite::get_all_tests() const
{
	return tests;
}

void TestSuite::add_test(const Test &test)
{
	tests.push_back(test);
}
