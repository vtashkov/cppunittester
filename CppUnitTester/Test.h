#pragma once
#include <string>
#include <functional>

struct Test
{
	std::string name;
	std::function<void()> test;
	Test(const std::string &name, const std::function<void()> &test) : name(name), test(test)
	{
	};
};
