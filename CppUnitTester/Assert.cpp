#include "Assert.h"
#include <exception>

void Assert::is_null(const void* expected)
{
	if (expected != nullptr)
		throw std::exception("Fail!");
}

void Assert::is_not_null(const void* expected)
{
	if (expected == nullptr)
		throw std::exception("Fail!");
}

void Assert::is_true(const bool& expected)
{
	if (!expected)
		throw std::exception("Fail!");
}

void Assert::is_false(const bool& expected)
{
	if (expected)
		throw std::exception("Fail!");
}
