#pragma once
#include <string>

class TestPresenter
{
public:
	virtual void start_tests(const int tests_count) = 0;
	virtual void start_test_suite(const std::string &name) = 0;
	virtual void start_test(const std::string &name) = 0;
	virtual void test_passed() = 0;
	virtual void test_failed(const std::string &message) = 0;
	virtual void end_test_suite() = 0;
	virtual void end_tests() = 0;
};
