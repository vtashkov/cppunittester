#include <iostream>
#include "ApplicationTests.h"
#include "TestPresenter.h"

ApplicationTests::ApplicationTests(TestPresenter &presenter) : presenter(presenter)
{
	suits = std::vector<TestSuite>();
}

void ApplicationTests::run()
{
	presenter.start_tests(get_all_tests_count());
	run_all_suits();
	presenter.end_tests();
}

void ApplicationTests::run_all_suits() const
{
	for (const auto& suite : suits)
		run_suit(suite);
}

void ApplicationTests::run_suit(const TestSuite & suite) const
{
	presenter.start_test_suite(suite.get_name());
	for (const auto& test : suite.get_all_tests())
		run_test(test);
	presenter.end_test_suite();
}

void ApplicationTests::run_test(const Test &test) const
{
	presenter.start_test(test.name);
	try
	{
	test.test();
	presenter.test_passed();
	}
	catch (...)
	{
		presenter.test_failed("Failing");
	}
}

void ApplicationTests::add_suite(const TestSuite& suit)
{
	suits.push_back(suit);
}

int ApplicationTests::get_all_tests_count()
{
	auto count = 0;
	for(const auto& suite : suits)
		count += suite.get_tests_count();
	return count;
}
