#pragma once

#include <functional>
#include <vector>

#include "Test.h"

class TestSuite
{
public:
	TestSuite(const std::string &name);
	std::string get_name() const;
	int get_tests_count() const;
	std::vector<Test> get_all_tests() const;
protected:
	void add_test(const Test &test);
private:
	std::string name;
	std::vector<Test> tests;
};

