#pragma once
#include "TestSuite.h"

#include <vector>
#include "TestPresenter.h"

class ApplicationTests
{
public:
	ApplicationTests(TestPresenter &presenter);
	void run();
protected:
	void add_suite(const TestSuite& suit);
private:
	TestPresenter &presenter;
	std::vector<TestSuite> suits;
	int get_all_tests_count();
	void run_all_suits() const;
	void run_suit(const TestSuite& suite) const;
	void run_test(const Test& test) const;
};

