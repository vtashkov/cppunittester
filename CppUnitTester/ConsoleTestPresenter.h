#pragma once

#include "TestPresenter.h"
#include <string>

class ConsoleTestPresenter : public TestPresenter
{
public:
	ConsoleTestPresenter();
	void start_tests(int tests_count);
	void start_test_suite(const std::string& name);
	void start_test(const std::string& name);
	void test_passed();
	void test_failed(const std::string& message);
	void end_test_suite();
	void end_tests();
private:
	int tests_count;
	int tests_executed;
	int tests_failed;
	std::string last_status_message;
	void erase_last_execution_message();
	void print_repeated_string(const std::string& s, int repeated) const;
	void print_status_message();
	std::string get_status_message() const;
	std::string get_executed_message() const;
	std::string get_errors_message() const;
	std::string get_status() const;
	static void print_message(const std::string& message);
};

