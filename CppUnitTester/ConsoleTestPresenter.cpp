#include "ConsoleTestPresenter.h"
#include <iostream>

ConsoleTestPresenter::ConsoleTestPresenter()
{
	tests_count = 0;
	tests_executed = 0;
	tests_failed = 0;
	last_status_message = "";
}

void ConsoleTestPresenter::start_tests(const int tests_count)
{
	this->tests_count = tests_count;
}

void ConsoleTestPresenter::start_test_suite(const std::string& name)
{
	erase_last_execution_message();
	const auto message = name + "\n";
	print_message(message);
}

void ConsoleTestPresenter::start_test(const std::string& name)
{
	erase_last_execution_message();
	const auto test_name_message = "\t" + name + "\n";
	print_message(test_name_message);
	print_status_message();
}

void ConsoleTestPresenter::test_passed()
{
	erase_last_execution_message();
	tests_executed++;
	print_status_message();
}

void ConsoleTestPresenter::test_failed(const std::string& message)
{
	erase_last_execution_message();
	tests_executed++;
	tests_failed++;
	print_status_message();
}

void ConsoleTestPresenter::end_test_suite()
{
}

void ConsoleTestPresenter::end_tests()
{
}

void ConsoleTestPresenter::erase_last_execution_message()
{
	print_repeated_string("\b", last_status_message.size());
	print_repeated_string(" ", last_status_message.size());
	print_repeated_string("\b", last_status_message.size());
	last_status_message = "";
}

void ConsoleTestPresenter::print_repeated_string(const std::string &s, const int repeated) const
{
	std::string print_string;
	for (auto i = 0; i < repeated; i++)
		print_string += s;
	print_message(print_string);
}

void ConsoleTestPresenter::print_status_message()
{
	auto status_message = get_status_message();
	print_message(status_message);
	last_status_message = status_message;
}

std::string ConsoleTestPresenter::get_status_message() const
{
	std::string status_message;
	status_message += get_executed_message();
	status_message += get_errors_message();
	status_message += get_status();
	return status_message;
}

std::string ConsoleTestPresenter::get_executed_message() const
{
	const auto tests_executed_string = std::to_string(tests_executed);
	const auto tests_count_string = std::to_string(tests_count);
	return "Executed " + tests_executed_string + " of " + tests_count_string + " tests. ";
}

std::string ConsoleTestPresenter::get_errors_message() const
{
	return tests_failed ? "(" + std::to_string(tests_failed) + " failed tests) " : "";
}

std::string ConsoleTestPresenter::get_status() const
{
	return !tests_failed ? "SUCCESS" : "ERROR";
}

void ConsoleTestPresenter::print_message(const std::string &message)
{
	std::cout << message;
}
